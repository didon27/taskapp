import React, { Component } from 'react';
import Nav from './components/Nav';
import LoginForm from './components/LoginForm';
import './App.css';
import qs from 'querystring';
import axios from 'axios';
import ListTasks from './components/ListTasks';
import TasksForm from './components/TasksForm';
const developer = 'work1'

const config = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayed_form: '',
      logged_in: localStorage.getItem('token') ? true : false,
      username: '',
      errors: ''
    };
  };


  new_tasks = (e, data) => {
    e.preventDefault();
    console.log(data);
    axios.post(`https://uxcandy.com/~shapoval/test-task-backend/v2/create/?developer=${developer}`, qs.stringify(data), config)
      .then(res => {
        console.log(res.data);
        this.setState({
          username: data.username
        });
        alert("Task successfully added");
      })
  };

  handle_login = (e, data) => {
    e.preventDefault();
    axios.post(`https://uxcandy.com/~shapoval/test-task-backend/v2/login/?developer=${developer}`, qs.stringify(data), config)
      .then(res => {
        console.log(res.data);
        console.log(res.data.status);
        if ((res.data.status = "ok")) {
          localStorage.setItem('token', res.data.message.token);
          localStorage.setItem('name', data.username);
          this.setState({
            logged_in: true,
            displayed_form: '',
            username: data.username
          });
        } else {

          this.setState({ errors: res.data.message });

        }
      })
  };

  handle_logout = () => {
    localStorage.removeItem('token');
    this.setState({ logged_in: false, username: '' });
  };

  display_form = form => {
    this.setState({
      displayed_form: form
    });
  };


  render() {
    let form;
    switch (this.state.displayed_form) {
      case 'login':
        form = <LoginForm errors={this.state.errors} handle_login={this.handle_login} />;
        break;
      default:
        form = null;
    }

    return (
      <div>
      <div className="header">
        <Nav
          logged_in={this.state.logged_in}
          display_form={this.display_form}
          handle_logout={this.handle_logout}
        />
        </div>
        <div className="App">
          {form}
          <h3>
            {this.state.logged_in
              ? `Hello, ${localStorage.getItem("name")}`
              : 'Please Log In'}
          </h3>
          <ListTasks logged_in={this.state.logged_in} />
          <TasksForm handle_login={this.new_tasks} />
      </div>
      </div>
    );
  }
}



export default App;
