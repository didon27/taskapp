import React from 'react';
import PropTypes from 'prop-types';

class TasksForm extends React.Component {
  state = {
    username: '',
    email: '',
    text: '',
    status: 0
  };

  handle_change = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevstate => {
      const newState = { ...prevstate };
      newState[name] = value;
      return newState;
    });
  };

  render() {
    return (
      <form id="TaskForm" onSubmit={e => this.props.handle_login(e, this.state)}>
        <h4>New Tasks</h4>
        <label htmlFor="username">Username</label>
        <input
          className="forms"
          type="text"
          name="username"
          value={this.state.username}
          onChange={this.handle_change}
        />
        <label htmlFor="email">Email</label>
        <input
          className="forms"
          type="email"
          name="email"
          value={this.state.email}
          onChange={this.handle_change}
        />
        <label htmlFor="text">Text</label>
        <input
          className="forms"
          type="text"
          name="text"
          value={this.state.text}
          onChange={this.handle_change}
        />
        <input type="submit" />
      </form>
    );
  }
}

export default TasksForm;

TasksForm.propTypes = {
  handle_login: PropTypes.func.isRequired
};
