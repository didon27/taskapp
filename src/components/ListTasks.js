import React, { Component } from 'react';
import axios from 'axios';
import qs from 'querystring';

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
};

class ListTasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            totalPages: "",
            currentPage: 1,
            pagesToShow: 5,
            sortField: "",
            sortDirection: "",
            id: "",
            value: ""
        };
        this.sortField = this.sortField.bind(this);
        this.sortDirection = this.sortDirection.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    updatePage() {
        axios.get(`https://uxcandy.com/~shapoval/test-task-backend/v2/?developer=work1&sort_field=${this.state.sortField}&sort_direction=${this.state.sortDirection}&page=${this.state.currentPage}`)
            .then(res => {
                this.setState({ list: res.data.message.tasks, totalPages: Math.floor(res.data.message.total_task_count / 3) });
            })
    };

    sortField(event) {
        this.setState({ sortField: event.target.value });
    };

    sortDirection(event) {
        this.setState({ sortDirection: event.target.value });
    };

    handleSubmit(event) {
        this.updatePage();
        event.preventDefault();
    };

    componentWillMount() {
        this.updatePage()
    };

    changesStatus = (id) => {
        var done = {
            token: localStorage.getItem('token'),
            status: 10,
            id: id
        }
        axios.post(`https://uxcandy.com/~shapoval/test-task-backend/v2/edit/${done.id}/?developer=work1`, qs.stringify(done), config)
            .then(res => {
                console.log(res.data)
            })
        this.updatePage();
    };

    changesText = (id) => {
        var done = {
            token: localStorage.getItem('token'),
            text: this.state.value,
            id: id
        }
        axios.post(`https://uxcandy.com/~shapoval/test-task-backend/v2/edit/${done.id}/?developer=work1`, qs.stringify(done), config)
            .then(res => {
                console.log(res.data)
            })
        this.updatePage();
    };

    componentWillReceiveProps(nextProps) {
        this.updatePage();
    };

    componentDidUpdate(prevProps, prevState) {
        if (this.state.currentPage !== prevState.currentPage) {
            this.setPage(this.state.currentPage);
            this.updatePage();
        }
    };

    setPage(page) {
        var { totalPages } = this.state;
        if (page < 1) {
            page = 1;
        } else if (page > totalPages) {
            page = totalPages;
        }
        this.setState({
            currentPage: page
        });
    };

    getPager() {
        var { pagesToShow, currentPage, totalPages } = this.state;
        var pages = [],
            startFromNumber;

        if (totalPages <= pagesToShow) {
            startFromNumber = 1;
            pagesToShow = totalPages;
        } else {
            if (currentPage <= Math.ceil(pagesToShow / 2)) {
                startFromNumber = 1;
            } else if (
                currentPage + Math.floor((pagesToShow - 1) / 2) >=
                totalPages
            ) {
                startFromNumber = totalPages - (pagesToShow - 1);
            } else {
                startFromNumber = currentPage - Math.floor(pagesToShow / 2);
            }
        }

        for (let i = 1; i <= pagesToShow; i++) {
            pages.push(startFromNumber++);
        }

        return {
            currentPage,
            totalPages,
            pages
        };
    };

    render() {
        var pager = this.getPager();
        return (
            <div >
                <form className="sort" onSubmit={this.handleSubmit}>

                    <select value={this.state.sortField} onChange={this.sortField}>
                        <option value="name">Name</option>
                        <option value="email">Email</option>
                        <option value="status">Status</option>
                    </select>

                    <select value={this.state.sortDirection} onChange={this.sortDirection}>
                        <option value="asc">asc</option>
                        <option value="desc">desc</option>
                    </select>
                    <input type="submit" value="Sort" />
                </form>
                {this.state.list.map(({ id, username, email, text, status }) => (
                    <div key={id} className='container'>
                        <span><div className="name">Name:</div> {username} </span>
                        <span><div className="name">Email:</div> {email}</span>
                        <span><div className="name">Text:</div> {this.props.logged_in
                            ? <div>
                                <form className="changeText" key={id} onSubmit={() => this.changesText(id)}>
                                    <input
                                        placeholder={text}
                                        key={id}
                                        onChange={(e) => this.setState({ value: e.target.value })}
                                    />
                                    <input type="submit" value="Edit" />
                                </form>
                            </div>
                            : text}</span>
                        <span>{status === 10 ? <div className="done">Done</div> : <div className="noDone">No done</div>}

                            {this.props.logged_in
                                ? <div>
                                    {status === 10 ? '' : <button className="button4" onClick={() => this.changesStatus(id)}>Done</button>}
                                </div>
                                : <div className="auth">Log in to change</div>}
                        </span>
                    </div>
                ))}
                <p>
                    Page {this.state.currentPage}/{this.state.totalPages}
                </p>
                <div className="pages">
                    <li>
                        <button
                            disabled={pager.currentPage === 1 ? true : false}
                            onClick={() => this.setPage(pager.currentPage - 1)}
                        >
                            &#8249;
          </button>
                    </li>

                    {pager.pages.map((page, index) => (
                        <li key={index}>
                            <button
                                className={pager.currentPage === page ? "active" : ""}
                                onClick={() => this.setPage(page)}
                            >
                                {page}
                            </button>
                        </li>
                    ))}

                    <li>
                        <button
                            disabled={pager.currentPage === pager.totalPages ? true : false}
                            onClick={() => this.setPage(pager.currentPage + 1)}
                        >
                            &#8250;
          </button>
                    </li>
                </div>
            </div>
        )
    }
}

export default ListTasks