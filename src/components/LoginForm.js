import React from 'react';
import PropTypes from 'prop-types';

class LoginForm extends React.Component {
  state = {
    username: '',
    password: '',
    errors: ''
  };

  handle_change = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState(prevstate => {
      const newState = { ...prevstate };
      newState[name] = value;
      return newState;
    });
  };

  componentWillReceiveProps(nextProps) {
      if (nextProps.errors) {
        this.setState({errors: nextProps.errors});
    }
  };

  render() {
    const { errors } = this.state;
    
    return (
      <form onSubmit={e => this.props.handle_login(e, this.state)}>
        <h4>Log In</h4>
        <label htmlFor="username">Username</label>
        <input
          className="forms"
          type="username"
          name="username"
          value={this.state.username}
          onChange={this.handle_change}
        />
        {errors.username && (<div className="invalid-feedback">{errors.username}</div>)}
        <label htmlFor="password">Password</label>
        <input
          className="forms"
          type="password"
          name="password"
          value={this.state.password}
          onChange={this.handle_change}
        />
 {errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
        <input type="submit" />
      </form>
    );
  }
}

export default LoginForm;

LoginForm.propTypes = {
  handle_login: PropTypes.func.isRequired
};
